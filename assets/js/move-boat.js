import axios from 'axios';

// function move boat
function moveBoat(url = null) {
    
    axios.post(url)
    .then(function (response) {

        if (response.data.message == 'success') {
            let victory = window.location.href;
            window.location.href = victory.replace('/map', '/victory');
        } else if (response.data.message == 'move') {
            const coordBoat = response.data.x + ',' + response.data.y;
            const coords = document.getElementsByClassName('coords');
            const boat = document.getElementById('boat');
            const tile = Array.prototype.filter.call(coords, function (tileName) {
                if (tileName.textContent == coordBoat) {
                    const parent = tileName.parentElement;
                    parent.appendChild(boat);
                    return true;
                };
            });
        }
        return false;
    }).
    catch(function (error) {
        window.alert("Il y a de la tempête et le bateau ne peut plus bouger !!!!");
    });
}

// move with buttons
document.querySelectorAll('a.js-move').forEach(function (move) {
    move.addEventListener('click', function (event) {
        event.preventDefault();
        let url = this.href;
        moveBoat(url);
    });
});

// move with keys of keyboard
document.addEventListener('keydown', function (event) {
    let direction = '';
    if (event.code === 'ArrowUp') {
        direction = 'N';
    } else if (event.code === 'ArrowDown') {
        direction = 'S';
    } else if (event.code === 'ArrowLeft') {
        direction = 'W';
    } else if (event.code === 'ArrowRight') {
        direction = 'E';
    }
    let str = window.location.href;
    let url = str.replace('/map', '/boat/direction/' + direction);
    moveBoat(url);
});