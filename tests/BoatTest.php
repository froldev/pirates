<?php

namespace App\Tests;

use App\Entity\Boat;
use PHPUnit\Framework\TestCase;

class BoatTest extends TestCase
{

    public function testGettersAndSetters(): void
    {
        //new Boat();
        $boat = new Boat();

        $this->assertNull($boat->getId());

        $boat->setName('test');
        $this->assertSame('test', $boat->getName());

        $boat->setCoordX(1);
        $this->assertSame(1, $boat->getCoordX());

        $boat->setCoordY(2);
        $this->assertSame(2, $boat->getCoordY());
    }

    public function testIsTrue()
    {
        //new Boat();
        $boat = new Boat();

        $boat->setName('test');
        $boat->setCoordX(1);
        $boat->setCoordY(2);

        $this->assertTrue($boat->getName() === 'test');
        $this->assertTrue($boat->getCoordX() === 1);
        $this->assertTrue($boat->getCoordY() === 2);
    }

    public function testIsFalse()
    {
        //new Boat();
        $boat = new Boat();

        $boat->setName('test');
        $boat->setCoordX(1);
        $boat->setCoordY(2);

        $this->assertFalse($boat->getName() === 'false');
        $this->assertFalse($boat->getCoordX() === 2);
        $this->assertFalse($boat->getCoordY() === 1);
    }

    public function testIsEmpty()
    {
        //new Boat();
        $boat = new Boat();

        $this->assertEmpty($boat->getName());
        $this->assertEmpty($boat->getCoordX());
        $this->assertEmpty($boat->getCoordY());
    }
}
