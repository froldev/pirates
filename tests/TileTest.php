<?php

namespace App\Tests;

use App\Entity\Tile;
use PHPUnit\Framework\TestCase;

class TileTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        //new Tile();
        $tile = new Tile();

        $this->assertNull($tile->getId());

        $tile->setType('test');
        $this->assertSame('test', $tile->getType());

        $tile->setCoordX(1);
        $this->assertSame(1, $tile->getCoordX());

        $tile->setCoordY(2);
        $this->assertSame(2, $tile->getCoordY());

        $tile->setHasTreasure(true);
        $this->assertSame(true, $tile->isHasTreasure());
    }

    public function testIsTrue()
    {
        //new Tile();
        $tile = new Tile();

        $tile->setType('test');
        $tile->setCoordX(1);
        $tile->setCoordY(2);
        $tile->setHasTreasure(true);

        $this->assertTrue($tile->getType() === 'test');
        $this->assertTrue($tile->getCoordX() === 1);
        $this->assertTrue($tile->getCoordY() === 2);
        $this->assertTrue($tile->isHasTreasure() === true);
    }

    public function testIsFalse()
    {
        //new Tile();
        $tile = new Tile();

        $tile->setType('test');
        $tile->setCoordX(1);
        $tile->setCoordY(2);
        $tile->setHasTreasure(true);

        $this->assertFalse($tile->getType() === 'false');
        $this->assertFalse($tile->getCoordX() === 2);
        $this->assertFalse($tile->getCoordY() === 1);
        $this->assertFalse($tile->isHasTreasure() === false);
    }

    public function testIsEmpty()
    {
        //new Tile();
        $tile = new Tile();

        $this->assertEmpty($tile->getType());
        $this->assertEmpty($tile->getCoordX());
        $this->assertEmpty($tile->getCoordY());
        $this->assertEmpty($tile->isHasTreasure());
    }
}
