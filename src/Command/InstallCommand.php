<?php

namespace App\Command;

use App\Entity\Boat;
use App\Entity\Tile;
use App\Repository\TileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:install',
    description: 'Create Map with Tiles, Boat and Treasure !',
)]
class InstallCommand extends Command
{
    public function __construct(
        private EntityManagerInterface $entityManagerInterface,
        private TileRepository $tileRepository,
    ) {
        $this->entityManagerInterface = $entityManagerInterface;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tiles = [
            [
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::PORT,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
            ],
            [
                $this->tileRepository::SEA,
                $this->tileRepository::PORT,
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
            ],
            [
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
            ],
            [
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
            ],
            [
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::PORT,
                $this->tileRepository::SEA,
            ],
            [
                $this->tileRepository::ISLAND,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::PORT,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::SEA,
                $this->tileRepository::ISLAND,
            ],
        ];

        $start = 0;
        foreach ($tiles as $y => $line) {
            foreach ($line as $x => $type) {
                $tile = new Tile();
                $tile->setType($type);
                $tile->setCoordX($x);
                $tile->setCoordY($y);
                if ($this->tileRepository::ISLAND === $type && 0 === $start) {
                    $tile->setHasTreasure(true);
                    ++$start;
                } else {
                    $tile->setHasTreasure(false);
                }
                $this->entityManagerInterface->persist($tile);
            }
        }

        $boat = new Boat();
        $boat->setCoordX(0);
        $boat->setCoordY(0);
        $boat->setName('Black Boat');
        $this->entityManagerInterface->persist($boat);

        $output->write('The Map is ready !');

        $this->entityManagerInterface->flush();

        return Command::SUCCESS;
    }
}
