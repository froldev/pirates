<?php

namespace App\Controller;

use App\Repository\BoatRepository;
use App\Service\MapService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/boat', name: 'boat_')]
class BoatController extends AbstractController
{
    public function __construct(
        private BoatRepository $boatRepository,
        private EntityManagerInterface $entityManager,
        private MapService $mapService
    ) {
    }

    #[Route('/move/{x}/{y}', name: 'move', requirements: ['x' => '\d+', 'y' => '\d+'])]
    public function index(int $x, int $y): Response
    {
        $boat = $this->boatRepository->findOneBy([]);
        $boat->setCoordX($x);
        $boat->setCoordY($y);

        $this->entityManager->flush();

        return $this->redirectToRoute('map');
    }

    #[Route('/direction/{direction}', name: 'update')]
    public function update(string $direction): Response
    {
        if (!in_array($direction, $this->boatRepository::DIRECTIONS)) {
            throw $this->createNotFoundException("Cette direction n'existe pas !");
        }

        $boat = $this->boatRepository->findOneBy([]);

        switch ($direction) {
            case $this->boatRepository::NORTH:
                $boat->setCoordY($boat->getCoordY() - 1);
                break;
            case $this->boatRepository::SOUTH:
                $boat->setCoordY($boat->getCoordY() + 1);
                break;
            case $this->boatRepository::EAST:
                $boat->setCoordX($boat->getCoordX() + 1);
                break;
            case $this->boatRepository::WEST:
                $boat->setCoordX($boat->getCoordX() - 1);
                break;
        }

        if ($this->mapService->tileExists($boat->getCoordX(), $boat->getCoordY())) {
            $this->entityManager->flush();
        }

        if ($this->mapService->checkTreasure($boat)) {
            return new JsonResponse([
                'message' => 'success',
            ]);
        }

        return new JsonResponse([
            'message' => 'move',
            'x' => $boat->getCoordX(),
            'y' => $boat->getCoordY(),
        ]);
    }
}
