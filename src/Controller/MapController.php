<?php

namespace App\Controller;

use App\Repository\BoatRepository;
use App\Repository\TileRepository;
use App\Service\MapService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MapController extends AbstractController
{
    #[Route('/map', name: 'map')]
    public function index(TileRepository $tileRepository, BoatRepository $boatRepository): Response
    {
        // Tiles
        $tiles = $tileRepository->findAll();

        foreach ($tiles as $tile) {
            $map[$tile->getCoordX()][$tile->getCoordY()] = $tile;
        }

        return $this->render('map/index.html.twig', [
            'map' => $map ?? [],
            'boat' => $boatRepository->findOneBy([]),
        ]);
    }

    #[Route('/start', name: 'start')]
    public function start(
        BoatRepository $boatRepository,
        TileRepository $tileRepository,
        EntityManagerInterface $entityManager,
        MapService $mapService
    ): Response {
        $boat = $boatRepository->findOneBy([]);

        if ($boat) {
            $boat
                ->setCoordX(0)
                ->setCoordY(0);

            $tile = $tileRepository->findOneBy([
                'hasTreasure' => true,
            ]);
            if ($tile) {
                $tile->setHasTreasure(false);
            }
            $mapService->getRandomIsland()->setHasTreasure(true);

            $entityManager->flush();
        }

        return $this->redirectToRoute('map');
    }
}
